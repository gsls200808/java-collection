package com.vvvtimes.collection.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetDemo {

    public void set() {
        System.out.println("set生成和遍历");
        Set set = this.genSet();
        System.out.println("for-each遍历");
        this.out(set);
        System.out.println("iterator遍历");
        this.out2(set);
        System.out.println("toString输出");
        this.out3(set);
    }

    public Set<String> genSet() {
        Set<String> set = new HashSet<String>();
        set.add("a");
        set.add("b");
        set.add("c");
        set.add("d");
        set.add("e");
        return set;
    }

    public void out(Set<String> set) {
        if (set != null) {
            for (String value : set) {
                System.out.println(value);
            }
        }
    }

    public void out2(Set<String> set) {
        if (set != null) {
            for (Iterator<String> iterator = set.iterator(); iterator.hasNext(); ) {
                System.out.println(iterator.next());
            }
        }
    }

    public void out3(Set<String> set) {
        if (set != null) {
            System.out.println(set);
        }
    }
}
