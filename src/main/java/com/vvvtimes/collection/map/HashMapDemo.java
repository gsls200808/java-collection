package com.vvvtimes.collection.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HashMapDemo {

    public void map(){
        System.out.println("map生成和遍历");
        Map map = this.genMap();
        System.out.println("for-entrySet遍历");
        this.out(map);
        System.out.println("for-keySet遍历");
        this.out2(map);
        System.out.println("Iterator-entrySet遍历");
        this.out3(map);
        System.out.println("Iterator-keySet遍历");
        this.out4(map);
    }

    public Map<Integer, String> genMap() {
        Map<Integer, String> map = new HashMap<Integer, String>();
        map.put(1, "aaa");
        map.put(2, "bbb");
        map.put(3, "ccc");
        return map;
    }

    public void out(Map<Integer, String> map) {
        if (map != null) {
            for (Map.Entry<Integer, String> entry : map.entrySet()) {
                System.out.println(entry.getKey() + "--->" + entry.getValue());
            }
        }
    }

    public void out2(Map<Integer, String> map) {
        if (map != null) {
            for (Integer key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }
        }
    }

    public void out3(Map<Integer, String> map) {
        if (map != null) {
            Iterator i = map.entrySet().iterator();
            while (i.hasNext()) {
                Map.Entry<Integer, String> entry1 = (Map.Entry<Integer, String>) i.next();
                System.out.println(entry1.getKey() + "--->" + entry1.getValue());
            }
        }
    }

    public void out4(Map<Integer, String> map) {
        if (map != null) {
            Iterator it = map.keySet().iterator();
            while (it.hasNext()) {
                Integer key = (Integer) it.next();
                String value = map.get(key);
                System.out.println(key + "--->" + value);
            }
        }
    }
}
