package com.vvvtimes.collection.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*
|--List:元素是有序的(怎么存的就怎么取出来，顺序不会乱)，元素可以重复（角标1上有个3，角标2上也可以有个3）因为该集合体系有索引，
        |-- ArrayList：底层的数据结构使用的是数组结构（数组长度是可变的百分之五十延长）（特点是查询很快，但增删较慢）线程不同步
        |-- LinkedList：底层的数据结构是链表结构（特点是查询较慢，增删较快）
        |-- Vector：底层是数组数据结构 线程同步（数组长度是可变的百分之百延长）（无论查询还是增删都很慢，被ArrayList替代了）
*/


public class ArrayListDemo {

    public void list() {
        System.out.println("ArrayList生成和遍历");
        List list = this.genList();
        System.out.println("for-each遍历");
        this.out(list);
        System.out.println("Iterator遍历");
        this.out2(list);
        System.out.println("for-list.size()遍历");
        this.out3(list);
        System.out.println("toString输出");
        this.out4(list);
    }

    public List<String> genList() {
        List<String> stringList = new ArrayList<String>();
        stringList.add("a");
        stringList.add("b");
        stringList.add("c");
        stringList.add("d");
        return stringList;
    }

    public void out(List<String> stringList) {
        if (stringList != null) {
            for (String str : stringList) {
                System.out.println(str);
            }
        }
    }

    public void out2(List<String> stringList) {
        if (stringList != null) {
            Iterator it = stringList.iterator();
            while (it.hasNext()) {
                String str = (String) it.next();
                System.out.println(str);
            }
        }
    }

    public void out3(List<String> stringList) {
        if (stringList != null) {
            for (int i = 0; i < stringList.size(); i++) {
                System.out.println(stringList.get(i));
            }
        }
    }

    public void out4(List<String> stringList) {
        if (stringList != null) {
            System.out.println(stringList);
        }
    }

}
