package com.vvvtimes.collection.list.vector;

import java.util.Enumeration;
import java.util.Stack;

//Stack<E> extends Vector<E>
//有部分开发者认为该类的设计存在问题，不建议使用java提供的Stack类
//第一个：vector底层用数组实现的，push 、pop 性能大大降低，Stack最好使用链表实现。
//第二个：这个类是java.util.Vector的子类，由于是继承，stack就会将父类的方法继承过来
//如：add(int index,E element)等（具体见Vector的API说明）。这个破坏了stack约定的规则（只能从栈顶进，栈顶出）。所以SUN公司自己也给出的解释就是不要轻易用这个类。
public class StackDemo {

    public void stack() {
        Stack stack = new Stack(); // 创建堆栈对象
        System.out.println("11111, absdder, 29999.3 三个元素入栈");
        stack.push(new Integer(11111)); //向 栈中 压入整数 11111
        printStack(stack);  //显示栈中的所有元素

        stack.push("absdder"); //向 栈中 压入
        printStack(stack);  //显示栈中的所有元素

        stack.push(new Double(29999.3)); //向 栈中 压入
        printStack(stack);  //显示栈中的所有元素

        String s = new String("absdder");
        System.out.println("元素absdder在堆栈的位置" + stack.search(s));
        System.out.println("元素11111在堆栈的位置" + stack.search(11111));

        System.out.println("11111, absdder, 29999.3 三个元素出栈"); //弹出 栈顶元素
        System.out.println("元素" + stack.pop() + "出栈");
        printStack(stack);  //显示栈中的所有元素
        System.out.println("元素" + stack.pop() + "出栈");
        printStack(stack);  //显示栈中的所有元素
        System.out.println("元素" + stack.pop() + "出栈");
        printStack(stack);  //显示栈中的所有元素
    }

    private static void printStack(Stack<Integer> stack) {
        if (stack.empty())
            System.out.println("堆栈是空的，没有元素");
        else {
            System.out.print("堆栈中的元素：");
            Enumeration items = stack.elements(); // 得到 stack 中的枚举对象
            while (items.hasMoreElements()) //显示枚举（stack ） 中的所有元素
                System.out.print(items.nextElement() + " ");
        }
        System.out.println(); //换行
    }
}
