package com.vvvtimes.collection.list;

import java.util.LinkedList;
import java.util.List;

public class LinkedListDemo {

    public void list(){
        System.out.println("LinkedList生成和遍历");
        List list = this.genList();
        System.out.println("for-each遍历");
        this.out(list);
    }

    public List<String> genList() {
        List<String> stringList = new LinkedList<String>();
        stringList.add("a");
        stringList.add("b");
        stringList.add("c");
        stringList.add("d");
        return stringList;
    }

    public void out(List<String> stringList) {
        for (String str : stringList) {
            System.out.println(str);
        }
    }
}
